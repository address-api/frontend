import {Component, OnInit} from '@angular/core';
import {CountryService} from './services/country.service';
import {StateService} from './services/state.service';
import {CityService} from './services/city.service';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';
import {Country} from './models/country';
import {State} from './models/state';
import {City} from './models/city';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Address API';
  public country: Country;
  public state: State;
  public city: City;

  private countries: Country[] = [];
  private states: State[] = [];
  private cities: City[] = [];

  constructor(private countryService: CountryService,
              private stateService: StateService,
              private cityService: CityService) {
  }

  ngOnInit(): void {
    this.loadCountries();
  }

  loadCountries() {
    this.countryService.getAllCountries()
      .subscribe(response => {
        this.countries = response;
      }, error => {
        console.log('Error loading countries: ' + error);
      });
  }

  countryFormatter = (country: Country) => country.name;
  stateFormatter = (state: State) => state.name;
  cityFormatter = (city: City) => city.name;

  searchCountry = (text$: Observable<string>) => text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    filter(term => term.length >= 2),
    map(term => this.countries.filter(country => new RegExp(term, 'mi').test(country.name)).slice(0, 10))
  );

  searchState = (text$: Observable<string>) => text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    filter(term => term.length >= 2),
    map(term => this.states.filter(state => new RegExp(term, 'mi').test(state.name)).slice(0, 10))
  );

  searchCity = (text$: Observable<string>) => text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    filter(term => term.length >= 2),
    map(term => this.cities.filter(city => new RegExp(term, 'mi').test(city.name)).slice(0, 10))
  );

  modelCountryChanged(country: Country) {
    if (country !== undefined) {
      console.log('Get state for country ' + country.id);
      this.loadStates(country);
    }
  }

  modelCityChanged(city: City) {
    console.log('City ' + JSON.stringify(city));
  }

  loadStates(country: Country) {
    this.stateService.getStateByCountryId(country.id)
      .subscribe(response => {
        this.states = response;
      }, error => {
        console.log('Error loading states: ' + error);
      });
  }

  loadCities(state: State) {
    this.cityService.getCityByStateId(state.id)
      .subscribe(response => {
        this.cities = response;
      }, error => {
        console.log('Error loading states: ' + error);
      });
  }


  modelStateChanged(state: State) {
    console.log('State ' + JSON.stringify(state));
    if (state !== undefined) {
      console.log('Load cities in state ' + state.name);
      this.loadCities(state);
    }
  }
}
