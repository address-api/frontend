import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {City} from '../models/city';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  cityByStateId = '/city/all?stateId=';

  constructor(private httpClient: HttpClient) {
  }

  getCityByStateId(stateId: number): Observable<City[]> {
    return this.httpClient.get<City[]>(environment.resourceUrl + this.cityByStateId + stateId);
  }
}
