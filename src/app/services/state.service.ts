import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {State} from '../models/state';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  stateByCountryId = '/state/all?countryId=';

  constructor(private httpClient: HttpClient) {
  }

  getStateByCountryId(countryId: number): Observable<State[]> {
    return this.httpClient.get<State[]>(environment.resourceUrl + this.stateByCountryId + countryId);
  }
}
